from telethon.sync import TelegramClient
from telethon.tl.functions.messages import GetAllStickersRequest, GetArchivedStickersRequest, GetStickerSetRequest
from telethon.tl.types import InputStickerSetID
from telethon.utils import guess_extension
import logging
import os

import config


STICKERS_DIR = "stickers"


logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


if not os.path.exists(STICKERS_DIR):
    os.mkdir(STICKERS_DIR)

with TelegramClient(config.name, config.api_id, config.api_hash) as client:
    sticker_sets = client(GetAllStickersRequest(0)).sets
    logger.info(f'Got {len(sticker_sets)} active sticker sets')

    offset_id = 0
    sticker_sets_archived = []
    while True:
        sticker_sets_tmp = client(GetArchivedStickersRequest(offset_id=offset_id, limit=100))

        if len(sticker_sets_tmp.sets) <= 0:
            break

        sticker_sets_archived += [set.set for set in sticker_sets_tmp.sets]
        offset_id = sticker_sets_archived[-1].id

        logger.info(f'Got {len(sticker_sets_archived)}/{sticker_sets_tmp.count} archived sticker sets')

    logger.info(f'Got {len(sticker_sets + sticker_sets_archived)} sticker sets total ({len(sticker_sets)} active, {len(sticker_sets_archived)} archived)')

    for i, set in enumerate(sticker_sets + sticker_sets_archived):
        print(f'[{set.title} ({set.short_name})](https://t.me/addstickers/{set.short_name})')

        logger.info(f'Downloading {i+1}/{len(sticker_sets + sticker_sets_archived)}')

        stickerpack_path = os.path.join(STICKERS_DIR, set.short_name)
        stickerpack_data_path = os.path.join(stickerpack_path, '.data')

        if not os.path.exists(stickerpack_path):
            os.mkdir(stickerpack_path)
        if not os.path.exists(stickerpack_data_path):
            os.mkdir(stickerpack_data_path)

        # Get the stickers for this sticker set
        stickers = client(GetStickerSetRequest(
            stickerset=InputStickerSetID(
                id=set.id, access_hash=set.access_hash
            )
        ))

        with open(os.path.join(stickerpack_data_path, 'data.json'), 'w') as file:
            file.write(stickers.to_json())

        for sticker in stickers.documents:
            sticker_file_name = str(sticker.access_hash) + guess_extension(sticker.mime_type)
            sticker_path = os.path.join(stickerpack_data_path, sticker_file_name)

            if not os.path.exists(sticker_path):
                client.download_media(sticker, file=open(sticker_path, 'wb'))

            for pack in stickers.packs:
                if sticker.id in pack.documents:
                    emoji = pack.emoticon

                    emoji_path = os.path.join(stickerpack_path, emoji)
                    if not os.path.exists(emoji_path):
                        os.mkdir(emoji_path)

                    sticker_emoji_path = os.path.join(emoji_path, sticker_file_name)
                    if not os.path.islink(sticker_emoji_path):  # Apparently a link doesn't "exist" according to Python
                        os.symlink(os.path.join('..', '.data', sticker_file_name),
                            sticker_emoji_path)
