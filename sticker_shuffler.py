from telethon.sync import TelegramClient
from telethon.tl.functions.messages import GetAllStickersRequest, GetArchivedStickersRequest, GetStickerSetRequest, InstallStickerSetRequest, ToggleStickerSetsRequest
from telethon.tl.types import InputStickerSetID
from telethon.tl.types.messages import StickerSetInstallResultArchive, StickerSetInstallResultSuccess
from telethon.utils import guess_extension
import logging
import os
import sys
import random
import time
from collections import OrderedDict

import config


logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


# Get force active stickers from file, use short name like in the url
force_active_stickersets = OrderedDict()
if len(sys.argv) > 1:
    for slug in open(sys.argv[1], 'r').readlines():
        force_active_stickersets[slug.strip()] = None
force_active_stickersets = list(force_active_stickersets.keys())

logger.info(f'Got {len(force_active_stickersets)} force active sticker sets')


with TelegramClient(config.name, config.api_id, config.api_hash) as client:
    sticker_sets = client(GetAllStickersRequest(0)).sets
    logger.info(f'Got {len(sticker_sets)} active sticker sets')

    # Get all stickers
    offset_id = 0
    sticker_sets_archived = []
    while True:
        sticker_sets_tmp = client(GetArchivedStickersRequest(offset_id=offset_id, limit=100))

        if len(sticker_sets_tmp.sets) <= 0:
            break

        sticker_sets_archived += [set.set for set in sticker_sets_tmp.sets]
        offset_id = sticker_sets_archived[-1].id

        logger.info(f'Got {len(sticker_sets_archived)}/{sticker_sets_tmp.count} archived sticker sets')

    logger.info(f'Got {len(sticker_sets + sticker_sets_archived)} sticker sets total ({len(sticker_sets)} active, {len(sticker_sets_archived)} archived)')

    # archive all active sticker sets
    while True:
        result = client(ToggleStickerSetsRequest(
            stickersets=[InputStickerSetID(id=stickerset.id, access_hash=stickerset.access_hash) for stickerset in sticker_sets],
            archive=True,
            uninstall=False,
            unarchive=False
        ))
        if result:
            break
        else:
            print(result.stringify())

    logger.info('Archived all stickers')

    # shuffle sticker sets with respect to the force active packs
    sticker_sets_lookup = {s.short_name: s for s in sticker_sets + sticker_sets_archived}
    stickersets = {s.short_name for s in sticker_sets + sticker_sets_archived}

    shuffled_stickersets = list(stickersets - set(force_active_stickersets))
    random.shuffle(shuffled_stickersets)
    active_stickersets = force_active_stickersets + \
        shuffled_stickersets[:200 - len(force_active_stickersets)]

    # activate sticker sets
    for i, stickerset_name in enumerate(list(active_stickersets)[::-1]):
        logger.info(f'Adding stickers ({i + 1}/{len(active_stickersets)}) {stickerset_name}')
        stickerset = sticker_sets_lookup[stickerset_name]

        while True:
            result = client(InstallStickerSetRequest(
                stickerset=InputStickerSetID(id=stickerset.id, access_hash=stickerset.access_hash),
                archived=False
            ))
            if type(result) == StickerSetInstallResultArchive or type(result) == StickerSetInstallResultSuccess:
                break
            else:
                print(result.stringify())

        # keep order on force active sticker packs
        if i > len(active_stickersets) - len(force_active_stickersets) - 5:
            time.sleep(1)

